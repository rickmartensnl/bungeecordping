package nl.rickmartens.bungeecordping.commands;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import nl.rickmartens.bungeecordping.BungeeCordPing;

public class PingCommand extends Command {

    public PingCommand() {
        super("ping");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if (!(commandSender instanceof ProxiedPlayer)) {
            return;
        }

        ProxiedPlayer player = (ProxiedPlayer) commandSender;

        String prefix = ChatColor.translateAlternateColorCodes('&', BungeeCordPing.pl.getMessageConfiguration().getString("prefix"));
        String yourping = prefix + ChatColor.translateAlternateColorCodes('&', BungeeCordPing.pl.getMessageConfiguration().getString("yourping"));
        String otherping = prefix + ChatColor.translateAlternateColorCodes('&', BungeeCordPing.pl.getMessageConfiguration().getString("otherping"));
        String notonline = prefix + ChatColor.translateAlternateColorCodes('&', BungeeCordPing.pl.getMessageConfiguration().getString("notonline"));

        if (args.length == 0) {
            int ping = player.getPing();
            yourping = yourping.replaceAll("%%ping%%", String.valueOf(ping));

            player.sendMessage(yourping);
            return;
        }

        ProxiedPlayer target = ProxyServer.getInstance().getPlayer(args[0]);

        if (target != null) {
            if (player != target) {
                int ping = target.getPing();
                otherping = otherping.replaceAll("%%ping%%", String.valueOf(ping));
                otherping = otherping.replaceAll("%%player%%", target.getDisplayName());

                player.sendMessage(otherping);
            } else {
                int ping = player.getPing();
                yourping = yourping.replaceAll("%%ping%%", String.valueOf(ping));

                player.sendMessage(yourping);
            }
        } else {
            player.sendMessage(notonline);
        }

    }

}