package nl.rickmartens.bungeecordping;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import nl.rickmartens.bungeecordping.commands.PingCommand;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

public final class BungeeCordPing extends Plugin {

    public static BungeeCordPing pl;
    private Configuration messageConfiguration;

    @Override
    public void onEnable() {
        pl = this;

        getProxy().getPluginManager().registerCommand(this, new PingCommand());

        if (!getDataFolder().exists()) {
            getDataFolder().mkdir();
        }

        File messageFile = new File(getDataFolder(), "messages.yml");

        if (!messageFile.exists()) {
            try (InputStream in = getResourceAsStream("messages.yml")) {
                Files.copy(in, messageFile.toPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            messageConfiguration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(messageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }

        getProxy().getConsole().sendMessage("[BungeeCordPing] Successful turned " + ChatColor.DARK_GREEN + "ON " + ChatColor.RESET + "BungeeCordPing v" + getDescription().getVersion());
    }

    @Override
    public void onDisable() {
        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(getMessageConfiguration(), new File(getDataFolder(), "messages.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        getProxy().getConsole().sendMessage("[BungeeCordPing] Successful turned " + ChatColor.DARK_RED + "OFF " + ChatColor.RESET + "BungeeCordPing v" + getDescription().getVersion());
    }

    public Configuration getMessageConfiguration() {
        return messageConfiguration;
    }

}
